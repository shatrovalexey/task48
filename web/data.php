<?php
	/**
	* @author Shatrov Aleksej <mail@ashatrov.ru>
	*
	* @const string IMAGE_PATH - папка к изображений
	* @const string IMAGE_EXT - расширение файов изображений
	* @const string IMAGE_TMP - временная папка для изображений
	* @const string IMAGE_FONT - путь к шрифту
	* @var array $colors - список с описанием цветов для анализа
	*/

	define( 'IMAGE_PATH' , 'img' ) ;
	define( 'IMAGE_EXT' , 'jpg' ) ;
	define( 'IMAGE_TMP' , '../.tmp' ) ;
	define( 'IMAGE_FONT' , '../ttf/arial.ttf' ) ;

	$colors = array(
		'черный' => array(
			'color' => 0x000000 ,
			'diff' => 0
		) ,
		'фиолетовый' => array(
			'color' => 0xEE82EE ,
			'diff' => 0
		) ,
		'синий' => array(
			'color' => 0x0000FF ,
			'diff' => 0
		) ,
		'зелёный' => array(
			'color' => 0x00FF00 ,
			'diff' => 0
		) ,
		'желтый' => array(
			'color' => 0xFFFF00 ,
			'diff' => 0
		) ,
		'оранжевый' => array(
			'color' => 0xFFA500 ,
			'diff' => 0
		) ,
		'красный' => array(
			'color' => 0xFF0000 ,
			'diff' => 0
		) ,
		'белый' => array(
			'color' => 0xFFFFFF ,
			'diff' => 0
		)
	) ;

	/**
	* заранее создаю папки
	*/
	@mkdir( IMAGE_TMP ) ;
	@mkdir( IMAGE_PATH ) ;

	header( 'Content-Type: application/json' ) ;

	/**
	* @var array $result - результаты обработки
	*/
	$result = array( ) ;

	try {
		/**
		* действия в зависимости от аргумента "action"
		*/
		switch ( @$_REQUEST[ 'action' ] ) {
			case 'upload' : { // загрузка изображений
				/**
				* заранее создаю папки
				*/
				foreach ( $colors as $color => &$value ) {
					@mkdir( IMAGE_PATH . '/' . $value[ 'color' ] ) ;
				}

				/**
				* @var array $url_parsed - разобранный аргумент "url"
				*/
				$url_parsed = parse_url( $_REQUEST[ 'url' ] ) ;
				$url_parsed[ 'scheme' ] = strToLower( $url_parsed[ 'scheme' ] ) ;
				$url_parsed[ 'host' ] = strToLower( $url_parsed[ 'host' ] ) ;

				/**
				* выход, если переданный URL использует неизвестный протокол
				*/
				if ( ! in_array( $url_parsed[ 'scheme' ] , array( 'http' , 'https' ) ) ) {
					throw new Exception( 'Invalid URL' ) ;
				}

				/**
				* @var \DOMDocument $domh - обработчик DOM
				*/
				$domh = new DOMDocument( '1.0' , 'utf-8' ) ;

				/**
				* загрузка URL
				*/
				if ( ! @$domh->loadHTMLFile( $_REQUEST[ 'url' ] ) ) {
					throw new Exception( 'Invalid URL' ) ;
				}

				/**
				* @var \DOMXPath $xpathh - обработчик страницы XPath
				* @var int $result[ 'count' ] - счётчик сохранённых изображений
				*/
				$xpathh = new DOMXPath( $domh ) ;
				$result[ 'count' ] = 0 ;

				// писк и просмотр //img/@src в HTML документе
				foreach ( $xpathh->query( '//img/@src' ) as $src ) {
					/**
					* @var array $src_parsed - разобранный URL картинки
					*/
					$src_parsed = parse_url( $src->value ) ;

					/**
					* пропуск, если path пуст
					*/
					if ( empty( $url_parsed[ 'path' ] ) ) {
						continue ;
					}

					$src_parsed[ 'scheme' ] = strToLower( @$src_parsed[ 'scheme' ] ) ;
					$src_parsed[ 'host' ] = strToLower( @$src_parsed[ 'host' ] ) ;

					/**
					* копирование атрибутов URL страницы в URL картинки
					*/
					foreach ( array( 'scheme' , 'host' , 'port' ) as $key ) {
						if ( ! empty( $src_parsed[ $key ] ) || empty( $url_parsed[ $key ] ) ) {
							continue ;
						}

						$src_parsed[ $key ] = $url_parsed[ $key ] ;
					}

					/**
					* @var string $src - результирующий URL картинки
					*/
					$src = $src_parsed[ 'scheme' ] . '://' . $src_parsed[ 'host' ] ;

					if ( ! empty( $src_parsed[ 'port' ] ) ) {
						$src .= ':' . $src_parsed[ 'port' ] ;
					}

					if ( $src_parsed[ 'path' ][ 0 ] != '/' ) {
						$src .= '/' . $url_parsed[ 'path' ] ;

						if ( $url_parsed[ 'path' ][ strlen( $url_parsed[ 'path' ] ) - 1 ] != '/' ) {
							$src .= '/' ;
						}
					}

					$src .= $src_parsed[ 'path' ] ;

					/**
					* @var resource $src_fh - файловый дескриптор картинки
					*/
					$src_fh = @fopen( $src , 'rb' ) ;

					if ( empty( $src_fh ) ) {
						continue ;
					}

					/**
					* @var string $dst_path - путь к временному файлу
					*/
					$dst_path = IMAGE_TMP . '/' . uniqid( ) ;

					/**
					* @var resource $dst_fh - файловый дескриптор временной картинки
					*/
					$dst_fh = fopen( $dst_path , 'wb' ) ;
					stream_copy_to_stream( $src_fh , $dst_fh ) ;
					fclose( $dst_fh ) ;
					fclose( $src_fh ) ;

					/**
					* @var string $dst_sha1 - SHA1 файла картинки
					*/
					$dst_sha1 = sha1_file( $dst_path ) ;

					/**
					* проверка дублирования изображения в имеющихся обработанных
					*/
					if ( @glob( IMAGE_PATH . '/*/' . $dst_sha1 . '.' . IMAGE_EXT ) ) {
						@unlink( $dst_sha1 ) ;

						continue ;
					}

					/**
					* @var string $img_fn - имя функции для открытия обработки изображения, в зависимости от типа изображения
					*/
					$img_fn = null ;

					/**
					* определение типа изображения
					*/
					switch( @exif_imagetype( $dst_path ) ) {
						case IMAGETYPE_GIF : {
							$img_fn = 'gif' ;

							break ;
						}
						case IMAGETYPE_JPEG : {
							$img_fn = 'jpeg' ;

							break ;
						}
						case IMAGETYPE_PNG : {
							$img_fn = 'png' ;

							break ;
						}
					}

					if ( empty( $img_fn ) ) {
						continue ;
					}

					$img_fn = 'imagecreatefrom' . $img_fn ;

					/**
					* @var resource $imh - дескриптор обрабатываемого изображения
					* @var int $imx - ширина изображения
					* @var int $imy - высота изображения
					*/
					$imh = @$img_fn( $dst_path ) ;
					$imx = @imagesx( $imh ) ;
					$imy = @imagesy( $imh ) ;

					if (
						empty( $imh ) ||
						( $imx < @$_REQUEST[ 'width' ] ) ||
						( $imx < @$_REQUEST[ 'height' ] )
					) {
						@unlink( $dst_path ) ;

						continue ;
					}

					/**
					* просмотр всех пикселей изображения и оценка их удалённости от цветов в $colors
					*/
					for ( $y = 0 ; $y < $imy ; $y ++ ) {
						for ( $x = 0 ; $x < $imx ; $x ++ ) {
							$color = imagecolorat( $imh , $x , $y ) ;

							foreach ( $colors as $key => &$value ) {
								$value[ 'diff' ] += abs( $value[ 'color' ] - $color ) ;
							}
						}
					}

					/**
					* @var string $max_color - название цвета, преобладающего в изображении
					* @var string $diff_color - кратчайший путь к цвету из $colors
					*/
					$max_color = $diff_color = null ;

					/**
					* поиск преобладающего в изображении цвета
					*/
					foreach ( $colors as $key => &$value ) {
						if ( ! is_null( $diff_color ) && ( $diff_color <= $value[ 'diff' ] ) ) {
							continue ;
						}

						$diff_color = $value[ 'diff' ] ;
						$max_color = $key ;
					}

					/**
					* @var int $imyw - высота изображения для изменения
					* @var int $imx2 - новая ширина изображения, пропорционально $imyw
					* @var int $imh2 - дескриптор холста для изображения с новыми габаритами
					* @var int $inv_color - цвет для текста на изображении
					*/
					$imyw = 200 ;
					$imx2 = $imx * ( $imyw / $imy ) ;
					$imh2 = imagecreatetruecolor( $imx2 , $imyw ) ;
					$inv_color = imagecolorallocate( $imh2 ,
						abs( 0xff - ( $colors[ $max_color ][ 'color' ] >> 16 ) & 0xff ) ,
						abs( 0xff - ( $colors[ $max_color ][ 'color' ] >> 8) & 0xff ) ,
						abs( 0xff - ( $colors[ $max_color ][ 'color' ] & 0xff ) )
					) ;

					/**
					* создание изображения с новыми размерами
					* создание подписи преобладающего текста на изображении
					* запись файла нового изображения
					* удаление временного файла
					* инкремент для результата
					*/
					imagecopyresampled( $imh2 , $imh , 0 , 0 , 0 , 0 , $imx2 , $imyw , $imx , $imy ) ;
					imagettftext( $imh2 , 17 , 0 , 5 , 30 , $inv_color , IMAGE_FONT , $max_color ) ;
					imagejpeg( $imh2 , IMAGE_PATH . '/' . $colors[ $max_color ][ 'color' ] . '/' . $dst_sha1 . '.' . IMAGE_EXT , 100.0 ) ;

					@unlink( $dst_path ) ;

					$result[ 'count' ] ++ ;
				}

				$result[ 'message' ] = 'загружено изображений: ' . $result[ 'count' ] ;

				break ;
			}
			case 'list' : { // создание списка файлов обработанных изображений
				foreach ( $colors as $color => &$value ) {
					$result[ sprintf( '%06X' , $value[ 'color' ] ) ] =
						@glob( IMAGE_PATH . '/' . $value[ 'color' ] . '/' . '*.' . IMAGE_EXT ) ;
				}

				break ;
			}
		}

		// вывод результата в фармате JSON
		echo json_encode( $result ) ;
	} catch( Exception $exception ) {
		echo json_encode( array(
			'message' => $exception->getMessage( )
		) ) ;
	}