jQuery( function( ) {
	let $processing = false ;
	let $form = jQuery( "#form" ) ;
	let $set_processing = function( $bool ) {
		$form.find( "input" ).prop( "disabled" , $bool ) ;
		$processing = $bool ;

		$form[ $bool ? "addClass" : "removeClass" ]( "processing" ) ;
	} ;
	let $message = $form.find( ".message-done" ) ;

	$form.on( "submit" , function( ) {
		if ( $processing ) {
			return false ;
		}

		$data = $form.serialize( ) ;

		$set_processing( true ) ;

		jQuery.ajax( {
			"dataType" : "json" ,
			"type" : $form.attr( "method" ) ,
			"url" : $form.attr( "action" ) ,
			"data" : $data ,
			"success" : function( $data ) {
				$set_processing( false ) ;
				$message.text( $data.message ) ;

				$reload( ) ;
			} ,
			"failure" : function( ) {
				$set_processing( false ) ;
			}
		} ) ;

		return false ;
	} ) ;

	let $reload = function( ) {
		if ( $processing ) {
			return ;
		}

		$set_processing( true ) ;

		let $list = jQuery( "#list" ).empty( ) ;

		jQuery.ajax( {
			"url" : $list.data( "src" ) ,
			"dataType" : "json" ,
			"success" : function( $data ) {
				$set_processing( false ) ;

				for ( let $color in $data ) {
					jQuery( $data[ $color ] ).each( function( ) {
						let $list_item = jQuery( "<li>" ) ;

						$list_item.css( {
							"background-image" : "url('" + this + "')" ,
							"border-color" : '#' + $color
						} ) ;

						$list.append( $list_item ) ;
					} ) ;
				}
			} ,
			"failure" : function( ) {
				$set_processing( false ) ;
			}
		} ) ;

		return false ;
	} ;

	jQuery( "#reload" ).on( "click" , $reload ).click( ) ;
} ) ;